<?php
namespace plugsystem\models;

use plugsystem\GlobalParams;

class NotifyModel
{
    public static function add($message)
    {
        $session=GlobalParams::$session;
        $array=$session->get("notifys");
        if (!is_array($array)) {
            $array=array();
        }
        $array[]=$message;
        $session->set("notifys", $array);
    }
    public static function getAll()
    {
        $session=GlobalParams::$session;
        $array=$session->get("notifys");

        if (!is_array($array)) {
            $array=array();
        }
        $session->delete("notifys");
        return $array;
    }
}
