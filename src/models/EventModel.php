<?php

namespace plugsystem\models;

use plugsystem\GlobalParams;

class EventModel {

    public static $events = array();

    protected static function add($event, $type, $method, $class) {
        $rows = EventModel::$events[$event];
        $canadd = true;
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row['type'] == $type and $row['method'] == $method and $row['class'] == $class) {
                    $canadd = false;
                    break;
                }
            }
        }
        if ($canadd) {
            EventModel::$events[$event][] = array('type' => $type, 'method' => $method, 'class' => $class);
        }
    }

    protected static function isExistEvents($class, $event, $method) {
        $events = $this->events;
        if (count($events)) {
            foreach ($events as $row) {
                if ($row['class'] = $class and $row['type'] == $type and $row['method'] == $method) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function init($obj) {
        $tmp_events = $obj->getEvents();



        if (count($tmp_events)) {
            foreach ($tmp_events as $event) {

                if (!isset(EventModel::$events[$event['event']])) {
                    EventModel::$events[$event['event']] = array();
                }
                EventModel::add($event['event'], $event['type'], $event['method'], $event['class']);
            }
        }
    }

    public static function isCan($event) {
        $isrun = GlobalParams::get($event . "_is_working");
        if (is_null($isrun)) {
            GlobalParams::set($event . "_is_working", true);
            return true;
        } else {
            if ($isrun === false) {
                GlobalParams::set($event . "_is_working", true);
                return true;
            } else {
                return false;
            }
        }
    }

    public static function run($event, $args) {
        $name_event = $event;
        if (isset(EventModel::$events[$event]) and count(EventModel::$events[$event])) {
            if (EventModel::isCan($event)) {
                foreach (EventModel::$events[$event] as $row) {

                    if ($row['type'] == "static") {
                        call_user_func_array($row['class'] . "::" . $row['method'], $args);
                    } else {
                        $tmpobj = new $row['class']();
                        call_user_func_array(array($tmpobj, $row['method']), $args);
                    }
                }
                GlobalParams::set($name_event . "_is_working", false);
            }
        }
    }

}
