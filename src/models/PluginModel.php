<?php

 namespace plugsystem\models;

use plugsystem\GlobalParams;

class PluginModel
{
    private $option;
    private $arr_query=array();
    private $plugins=array();
    public function __construct($option, $folder="frontend")
    {
        $option['folder']=$folder;
        $this->option=$option;





        if (isset($this->option['additional_namespaces'])) {
            $additional_namespaces=$this->option['additional_namespaces'];
           

            if (is_array($additional_namespaces) and count($additional_namespaces)) {
                foreach ($additional_namespaces as $namespace) {
                    $this->addPlugin($namespace."\\config");
                }
            }
        }
        $this->scan_directory($this->option['plugins_directory']);
    }
   private function scan_directory($directory)
    {
        $dir=scandir($directory);
        if (count($dir)) {
            foreach ($dir as $plugin) {
                if (file_exists($directory."/".$plugin."/config.php")) {
                    if (strlen($this->option['plugins_namespace'])==0) {
                        $this->addPlugin($this->option['plugins_namespace'].'\\'.$plugin.'\\config');
                    } else {
                        $this->addPlugin('\\'.$this->option['plugins_namespace'].'\\'.$plugin.'\\config');
                    }
                }
            }
        }
    }
    private function addPlugin($config_class)
    {
        if (class_exists($config_class)) {
            $class = $config_class;
            $config = new $class();
            EventModel::init($config);
            $this->plugins[]=$config;
        }
    }
    private function getAll()
    {
    }
    private function getFirst($query_string)
    {
        $tmp=explode("/", $query_string);
        if (isset($tmp[1])) {
            $array=array();
            $array['first']=$tmp[1];
            $i=2;
            $additional="";
            while (isset($tmp[$i])) {
                $nexti=$i+1;
                if (isset($tmp[$nexti])) {
                    $additional.=$tmp[$i]."/";
                } else {
                    $additional.=$tmp[$i];
                }
                $i++;
            }
            $array['for_plugin']=$additional;
            $this->arr_query=$array;
            return $tmp[1];
        } else {
            return null;
        }
    }
    public function find($query_string)
    {
        $result=null;
        $nameroute=$this->getFirst($query_string);
        if (is_null($nameroute)) {
            return null;
        }



        if (count($this->plugins)) {
            foreach ($this->plugins as $config) {
                EventModel::init($config);

                if ($config->getRoute()==$nameroute and $config->isEnable()) {
                    $result=$config->findResult($this->arr_query['for_plugin'], $this->option);
                }
            }
        }

        return $result;
    }
}
