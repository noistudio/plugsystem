<?php

 namespace plugsystem\models;

use plugsystem\GlobalParams;

class ViewModel
{
    protected $path;
    protected $data;

    public function __construct($path, $data = array())
    {
        $this->path = $path;
        $this->data = $data;
        $params=GlobalParams::params();

        if (isset($params['type']) and  $params['type']!="cron") {
            $this->data['_csrf']=GlobalParams::$helper->c("csrf")->get();
        }
         
        
    }




    public function render()
    {
        if (file_exists($this->path)) {
            ob_start();


            if (count($this->data)) {
                foreach ($this->data as $key=>$var) {
                    $this->$key=$var;
                }
            }
            @include($this->path);

            $output = ob_get_clean();
            $params=GlobalParams::params();
            if (isset($params['type']) and $params['type'] == "backend"
            ) {
                $pathadmin = GlobalParams::$helper->link('');
                $output = str_replace("{pathadmin}", $pathadmin, $output);
            } else {
                $pathadmin = GlobalParams::$helper->link('');
                $output = str_replace("{path_" . $params['type'] . "}", $pathadmin, $output);
            }
            $folder_path=$params['theme_path'];
            if (isset($_SERVER['DOCUMENT_ROOT'])) {
                $folder_path=str_replace($_SERVER['DOCUMENT_ROOT'], "", $folder_path);
            }
            $output=str_replace("{asset}", $folder_path, $output);
            return $output;
        }
    }

    // public function __get($name)
    // {
    //     if (isset($this->data[$name])) {
    //         return $this->data[$name];
    //     } else {
    //         return null;
    //     }
    // }
}
