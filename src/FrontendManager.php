<?php

namespace plugsystem;

use plugsystem\models\PluginModel;
use plugsystem\models\ViewModel;

class FrontendManager extends \plugsystem\core\AbstractManager
{
    protected $option=array(

  'path_to_file'=>false,
  'basic_route'=>'/',
  'access'=>'',
  'plugins_namespace'=>'plugins',
  'plugins_directory'=>'',
  'theme_path'=>'',
  'query_string'=>'',
  'session_class'=>'\\plugsystem\models\\SessionModel',
  'namespace_components'=>'\\plugcomponents\\',
  'default_paginator_file'=>__DIR__.'/../paginator.php'

  );

    public function __construct($option_array=array())
    {
        $option_array['type']="frontend";
        $option_array['query_string']=$this->replaceurls($option_array['query_string'], $option_array['theme_path']);

        parent::__construct($option_array);
    }
    /*
    функция которая заменяет альтернативные ссылки на настоящие 
    пример файла urls.json
    
    [
  {"url":"/content/page/1","newurl":"/aboutus"}
]

 в случае если newtoold=true
 то newurl заменяет на url
 если false то наоборот
    */
    private function replaceurls($content, $path, $newtoold=true)
    {
        if (file_exists($path."/urls.json")) {
            $json=json_decode(file_get_contents($path."/urls.json"), true);


            if (is_array($json)) {
                foreach ($json as $arr) {
                    if (isset($arr['url']) and isset($arr['newurl'])) {
                        if ($newtoold) {
                            $content=str_replace($arr['newurl'], $arr['url'], $content);
                        } else {
                            $content=str_replace($arr['url'], $arr['newurl'], $content);
                        }
                    }
                }
            }
        }

        return $content;
    }

    protected function render($result)
    {
        $file="main.php";
        if (is_null($result) and ($this->option['query_string']!="/" and $this->option['query_string']!="/404")) {
            header("Location: /404");
            die();
        }
        if ($this->option['query_string']=="/404") {
            $file="404.php";
        }
        $data=array();
        $data['plugin']=$result;
        $view=new ViewModel($this->getViewPath($file), $data);
        $result=$this->replaceurls($view->render(), $this->option['theme_path'], false);

        return $result;
    }
}
