<?php

namespace plugsystem;

use plugsystem\models\ViewModel;

class GlobalParams
{
    public static $array=array();
    public static $session=null;
    public static $helper=null;

    public static function get($key)
    {
        if (isset(GlobalParams::$array[$key])) {
            return GlobalParams::$array[$key];
        } else {
            return null;
        }
    }
    public static function params()
    {
        return GlobalParams::get("main_setting");
    }
    public static function set($key, $val)
    {
        GlobalParams::$array[$key]=$val;
    }
    /*
    
    аналог GlobalParams::$helper->c();
    */
    public static function getDocumentRoot()
    {
        $dir=__DIR__;

        $path=realpath($dir."/../../../../../");


        return $path;
    }
    
      public function c($name)
    {
        return GlobalParams::$helper->c($name);
    }
    
      public static function setTitle($title)
    {
        GlobalParams::set("site_title", $title);
    }
    public static function getTitle()
    {
        $title=GlobalParams::get("site_title");
        if (is_null($title)) {
            return "";
        } else {
            return $title;
        }
    }
    public static function setKeywords($keywords)
    {
        GlobalParams::set("site_keywords", $keywords);
    }

    public static function getKeywords()
    {
        $string=GlobalParams::get("site_keywords");
        if (is_null($string)) {
            return "";
        } else {
            return $string;
        }
    }
    public static function setDescription($description)
    {
        GlobalParams::set("site_description", $description);
    }
    

    public static function getDescription()
    {
        $string=GlobalParams::get("site_description");
        if (is_null($string)) {
            return "";
        } else {
            return $string;
        }
    }    
    
  public static function render($file, $data, $inner=true)
    {
        $option=GlobalParams::get("main_setting");
        $file=str_replace(".php", "", $file);
        $file=$file.".php";

        if ($inner) {
            $path=$option['theme_path']."".$file;
        } else {
            $path=$file;
        }

        $view=new ViewModel($path, $data);
        return $view->render();
    }
}
