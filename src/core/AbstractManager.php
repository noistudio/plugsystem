<?php

namespace plugsystem\core;

use plugsystem;
use plugsystem\foradmin\UserAdmin;
use plugsystem\models\EventModel;
use plugsystem\models\PluginModel;
use plugsystem\models\ViewModel;
use plugsystem\GlobalParams;

abstract class AbstractManager
{
    protected $option=array(

  'path_to_file'=>false,
  'basic_route'=>'/',
  'access'=>'/pathtoadmin/',
  'plugins_namespace'=>'plugins',
    'plugins_directory'=>'',
  'theme_path'=>'',
  'query_string'=>'',
  'session_class'=>'\\plugsystem\models\\SessionModel',
  'namespace_components'=>'\\plugcomponents\\',
  "additional_namespaces"=>array()


  );
    protected $plugin_model=null;
    public function getViewPath($file)
    {
        $path=$this->option['theme_path']."".$file;

        return $path;
    }
    public function __construct($option_array=array(), $checktheme=true)
    {
        $iserror=1;
        $this->option=array_merge($this->option, $option_array);

        $this->option['vendor_path']=realpath(__DIR__."/../");

        if (strlen($this->option['plugins_directory'])==0) {
            throw new \Exception('You not set plugins_directory');
        } else {
            if (!file_exists($this->option['plugins_directory'])) {
                throw new \Exception('plugins_directory not exists!');
            } else {
                $iserror=0;
            }
        }
        if ($checktheme) {
            if (strlen($this->option['theme_path'])==0) {
                $iserror=1;
                throw new \Exception('You not set theme_path');
            } else {
                if (!file_exists($this->option['theme_path'])) {
                    $iserror=1;
                    throw new \Exception('theme_path not exists!');
                } else {
                    $iserror=0;
                }
            }
        }
         $this->option['query_string']=$this->filter($this->option['query_string']);
        GlobalParams::set("main_setting", $this->option);
        GlobalParams::$session=new $this->option['session_class'];
        GlobalParams::$helper=new \plugsystem\foradmin\Helper;
        $this->plugin_model=new PluginModel($this->option, $this->option['type']);
    }
   public function filter($url)
    {
        $query_string=$url;
        $query_string=str_replace(".html", "", $query_string);

        $query_string=explode("?", $query_string);
        if (isset($query_string[0])) {
            $url=$query_string[0];
        }
        return $url;
    }
    public function start($onlyresult=false)
    {
        $return_result=null;


        $result=$this->plugin_model->find($this->option['query_string']);

        if ($onlyresult==true) {
            $return_result=$result;
        } else {
            $return_result=$this->render($result);
        }

        GlobalParams::set("result_render", $return_result);
        EventModel::run("before_return", array());
        $return_result=GlobalParams::get("result_render");

        return $return_result;
    }

    abstract protected function render($result);
}
