<?php

namespace plugsystem\core;

use plugsystem\GlobalParams;
use plugsystem\models\ViewModel;

class Controller
{
    public $config=null;
    private $path=null;
    protected $option=null;

    public function __construct()
    {
        $this->option=GlobalParams::get("main_setting");
        $this->config=$this->getConfig();
        $this->path=$this->getPath();
    }
    private function getPath() {
        $theme_path = $this->option['theme_path'];
        $nameclass = get_class($this);
        $explode = explode('\\', $nameclass);
        $class = $explode[count($explode) - 1];

        $checkClass = preg_replace('/([a-z0-9])?([A-Z])/', '$1-$2', $class);
        $explode_subs = explode("-", $checkClass);
        if (count($explode_subs) > 0) {
            $normal_folders = array();
            foreach ($explode_subs as $sub) {
                if (strlen($sub) > 1) {
                    $normal_folders[] = $sub;
                }
            }
            if (count($normal_folders) > 0) {
                $normal_folders = array_reverse($normal_folders);

                $tmp_path = implode("/", $normal_folders);
                $tmp_path = strtolower($tmp_path);
                $result_path = $theme_path . "/plugin/" . $tmp_path;
                $result_path = str_replace("//", "/", $result_path);

                return $result_path;
            }
        }
        $class = strtolower($class);
        $newpath = explode($this->config->getName(), $class);


        $result_path = "";
        if ($newpath[0] == "" and $newpath[1] == "") {
            $result_path = $theme_path . "/plugin/" . $this->config->getName();
        } else {
            $result_path = $theme_path . "/plugin/" . $this->config->getName() . "/" . $newpath[0];
        }
        $result_path = str_replace("//", "/", $result_path);
        return $result_path;
    }
    public function isExists($file)
    {
        $path=$this->path."/".$file.".php";
        return file_exists($path);
    }
    private function getConfig()
    {
        $first_class=get_called_class();
        $explode=explode("\controllers", $first_class);
        if ($explode[0]) {
            $class=$explode[0].'\\config';
            $obj=new $class;

            return $obj;
        }
    }
    public function render($file, $data)
    {
        $file=str_replace(".php", "", $file);
        $path=$this->path."/".$file.".php";

        $view=new ViewModel($path, $data);
        return $view->render();
    }

    public function path($file)
    {
        $file=str_replace(".php", "", $file);
        $path=$this->path."/".$file.".php";
        return $path;
    }
}


