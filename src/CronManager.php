<?php

namespace plugsystem;

use plugsystem\models\PluginModel;
use plugsystem\models\ViewModel;

class CronManager extends \plugsystem\core\AbstractManager
{
    protected $option=array(

  'path_to_file'=>false,
  'basic_route'=>'/',
  'access'=>'',
  'plugins_namespace'=>'plugins',
  'theme_path'=>'',
  'query_string'=>'',
  'session_class'=>'\\plugsystem\models\\SessionModel',
  'namespace_components'=>'\\plugcomponents\\',
  'default_paginator_file'=>__DIR__.'/paginator.php'

  );

    public function __construct($option_array=array())
    {
        $option_array['type']="cron";
        parent::__construct($option_array, false);
    }

    protected function render($result)
    {
        return $result;
    }
}
